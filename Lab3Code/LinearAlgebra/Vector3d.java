//Sophia Marshment - 2038832

public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return this.x;
    }
    public double getY() {
        return this.y;
    }
    public double getZ() {
        return this.z;
    }

    public double magnitude() {
        double magnitude;
        magnitude = Math.sqrt((this.x*this.x)+(this.y*this.y)+(this.z*this.z));

        return magnitude;
    }

    public double dotProduct(Vector3d vector) {
        double product;
        product = (this.x*vector.x)+(this.y*vector.y)+(this.z*vector.z);

        return product;
    }

    public Vector3d add(Vector3d vector) {
        Vector3d newVector = new Vector3d(this.x+vector.x,this.y+vector.y,this.z+vector.z);
        return newVector;
    }
}