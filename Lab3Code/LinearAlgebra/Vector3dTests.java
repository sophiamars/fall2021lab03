//Sophia Marshment - 2038832

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class Vector3dTests {

    @Test
    void getXTest() {
        Vector3d vector = new Vector3d(1, 2, 3);
        assertEquals(1, vector.getX());
    }
    @Test
    void getYTest() {
        Vector3d vector = new Vector3d(1, 2, 3);
        assertEquals(2, vector.getY());
    }
    @Test
    void getZTest() {
        Vector3d vector = new Vector3d(1, 2, 3);
        assertEquals(3, vector.getZ());
    }

    @Test
    void magnitudeTest() {
        Vector3d vector = new Vector3d(1, 2, 3);
        assertEquals(3.7, vector.magnitude(), 1);
    }

    @Test
    void dotProductTest() {
        Vector3d vector1 = new Vector3d(1, 2, 3);
        Vector3d vector2 = new Vector3d(4, 5, 6);

        double product = vector1.dotProduct(vector2);
        assertEquals(32, product);
    }

    @Test
    void addTest() {
        Vector3d vector1 = new Vector3d(1, 2, 3);
        Vector3d vector2 = new Vector3d(4, 5, 6);

        Vector3d vector3 = vector1.add(vector2);
        assertEquals(4, vector3.getX());
        assertEquals(7, vector3.getY());
        assertEquals(9, vector3.getZ());
    }
}